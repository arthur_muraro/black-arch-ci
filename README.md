# Harder-OS

## Contributeurs 
- MURARO Arthur
- RANDRIANASOLO Gaëtan 

## Idée

Créer une Machine Virtuelle Linux customisé pour des tâches de pentest. Intégrer le build de cette machine à une CI pour la rebuild à jour toutes les semaines, et la rendre disponible partout.

## La mise en oeuvre

Nous avons d'abord pensé à une mise en oeuvre en utilisant Nix combiné à Ansible (*probablement*) et enfin packer pour l'ISO finale, ou alors créer un script de building manuellement en bash.

~~Après plusieurs recherches, nous allons tenter un premier essai avec Terraform, intégré à la CI Gitlab et Packer. Packer buildera une image, Terraform servira de runner sur le dépot, pour refaire une image à partir du template packer, toutes les semaines. Enfin Ansible permettra de mettre en place la configuration du système.~~

~~Si nous avons le temps, nous essayerons aussi de nous casser la tête sur Nix qui semble pouvoir tout faire tout seul une fois bien maitrisé.~~

Au final, nous utilisons packer pour créer une VM avec une cloud-init install. Nous utilisons ansible pour provisionner les configurations puis enfin gitlab CI pour la mise en oeuvre des taches de building.

Le tout permettra à partir d'un template Vagrant, de pull l'image buildé et de créer des VMs toujours à jour.

## Le déploiement de l'OS

La VM sera construite en quatre étapes :
- **core**: La couche servant au bon fonctionnement de Arch.
- **graphic**: La couche relative à l'environement utilisateur, par exemple la partie graphique du système, les additions invités, les éditeurs, etc....
- **applications**: La couche déployant les outils personnalisés ainsi que les dépots Black Arch pour les outils de pentest.
- **user** La dernière couche, elle concerne la configuration personnelle des utilisateurs, leur environement, applis personalisées, et biensûr dotfiles.

Ce système nous permettra à la fois de faciliter le maintiens des scripts pour chaque couche, mais aussi de faciliter le debugging lors du déploiement. Enfin, il sera facile pour un utilisateur de forker le dépot et customiser les couches dont il ne veut pas, comme **applicatoins** et **users**

- Identifiants par défaut : `pentester:pentester`

## Build manuel de la VM pour virtualbox (ou autre depuis qcow2)

```bash
packer init .
packer build --var username=pentester --var password=pentester .
VBoxManage convertfromraw output-archlinux/golden-arch.qcow2 output-archlinux/golden-arch.vdi
```

## Gitlab CI

Après beaucoup de difficultées rencontrées, l'idée d'une CI executé par les serveurs gitlab pour build l'image n'est pas possible si nous n'avons pas notre propre serveur hébergé capable de faire de la virtualisation. Nous avons donc transformé la CI pour qu'elle soit lancé en local lorsque l'utilisateur  "push" sur le dépot. Le job Gitlab CI est lancé à partir d'un Hook git.

L'utilitaire capable de lire et exécuter en local les job gitlab CI s'appelle [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local).

### build le vdi pour virtualbox

```bash
git checkout vbox
cp pre-push .git/hooks/
chmod +x .git/hooks/pre-push
# puis faire un push sur le repo pour trigger le hook
```

## Harder Docker

Pour palier à ces problématique, nous avons aussi transformé notre VM en container docker. Le build reste sensiblement le même, mis à part la couche "graphic" qui est supprimée.

### Build l'image

`git checkout docker` puis faire un push sur le repo pour trigger le runner Gitlab CI.

### Lancer l'image

```bash
docker pull registry.gitlab.com/arthur_muraro/harder-os 
docker tag registry.gitlab.com/arthur_muraro/harder-os arch-pentest
docker image rm registry.gitlab.com/arthur_muraro/harder-os 
docker run -it --rm --net=host --ipc=host --privileged -e DISPLAY=$DISPLAY -u pentester arch-pentest
```