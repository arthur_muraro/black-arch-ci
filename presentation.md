---
marp: true
theme: uncover
---

# Harder OS
*Un OS personnalisé sur un téléphone c'est bien, sur un ordi c'est mieux (**HEIN SACHA !**)*

---

### 1. Besoin d'un environnement de pentest
- Stable
- À jour
- Distribué
### 2. Envie de taffer sur de la CI et de tester Packer

---

![h:650](./images/CI.svg)

---

## ![h:85](https://about.gitlab.com/images/press/logo/png/gitlab-logo-100.png)

#

Outils de déploiement et intégration continue intégré à gitlab

#

```yml
build:
  stage: Hack The Planet
  script:
    - packer HackThePlanet ( automatically )
```

---

## ![h:110](https://i.pinimg.com/originals/f3/84/94/f3849411fddd2df45b3d4387f5339ee9.png)

Provisonneur d'infrastructure automatisé *as code*

```json
source "qemu" "archlinux" {
  iso_url          = "https://admin.tryhackme.com/arch.iso"
  memory           = 8000
  cpus             = 4
  disk_size        = "15000M"
}

build {
  sources = ["source.qemu.archlinux"]

  provisioner "shell" {
    inline = ["ansible HackThePlanet.yml ( as code )"]
  }
```

---

## ![h:200](https://cdn.icon-icons.com/icons2/2699/PNG/512/ansible_logo_icon_169596.png)

Automatiseur et gestionnaire de configuration système

###

```yml
- name: Hack The Planet
  become: true
  shell: |
    echo 'Hack the planet !!' ( but in ssh )
```

### 
###
###

---

### Arch from *( presque )* scratch
- environement Arch
- Paquets
- services
- serveur graphique
- Black Arch

---

### Déploiement de Arch avec *(presque)* exclusivement Ansible
---

![h:650](./images/vbox.jpg)

---

### *( même pas presque )* Monter la CI Gitlab
- VirtualBox
- Docker
- VDS

**Solution : gitlab-ci-local + docker dind**

---

# Améliorations possible ?


1. Améliorer la qualité des builds avec un Meilleure CI (debug, linters, typos)
2. Mettre en place un serveur auto hébergé pour pouvoir réellement build automatiquement et rendre la VM disponible depuis internet


---

#### Building d'un container de pentest via Docker 
- Réutilisation du code Ansible déjà produit
- Réduction en temps et ressources du build
- Réduction en ressources à l'utilisation
- Réutilisation de la CI Gitlab pour pouvoir la rendre *Remote*

---

![h:650](./images/docker.jpg)

---

# Demo !